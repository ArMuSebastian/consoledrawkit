// swift-tools-version:5.5
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

private let tableDrawingTargetName = "TableDrawingKit"

private let packageName = "ConsoleDrawKit"
private let drawingLibraryName = packageName
private let drawingTargetName = packageName

let package = Package(
    name: packageName,
    products:
        [
            .library(
                name: drawingLibraryName,
                targets:
                    [
                        drawingTargetName
                    ]
            ),
        ],
    targets:
        [
            .target(
                name: tableDrawingTargetName
            ),
            .target(
                name: drawingTargetName,
                dependencies:
                    [
                        .target(name: tableDrawingTargetName)
                    ]
            ),
        ]
)
