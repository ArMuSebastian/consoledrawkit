//
//  Table.swift
//  
//
//  Created by Artem Myshkin on 04.03.2022.
//

import Foundation

public struct Table {

    let accessory: Accessory
    let characterSet: TableDrawingCharacterSet

    public init(
        accessory: Accessory = .init(),
        characterSet: DrawingCharacterSet = .first
    ) {
        self.accessory = accessory
        self.characterSet = characterSet.characterSet
    }

    public func string(
        from data: [[CustomStringConvertible]]
    ) -> String {

        let accessoriedData = accessoriedData(data: data)

        let maxElementsInRow = accessoriedData
            .map(\.count)
            .reduce(0, max)

        let normalizedData = normalisedData(data: accessoriedData)
        let infos = normalizedData
            .map { rowNormalizedData -> Info in
                let kekw = rowNormalizedData
                    .map(String.init(describing:))
                let widths: [Int] = kekw.map(\.count)
                let strings: [String] = kekw
                return Info(rowStrings: strings, widths: widths)
            }

        let colWidths = infos
            .reduce(into: Array(repeating: 0, count: maxElementsInRow)) { result, info in
                info.widths
                    .enumerated()
                    .forEach { index, value in
                    result[index] = max(result[index], value)
                }
            }

        let strings = infos.map { info in
            info.rowStrings
                .enumerated()
                .map { index, string -> String in
                    let desiredWidth = colWidths[index]
                    return string + String(repeating: " ", count: desiredWidth - string.count)
                }
        }

        let top         = line(with: colWidths, type: .top)
        let bottom      = line(with: colWidths, type: .bottom)
        let inside      = line(with: colWidths, type: .inside)
        let contentRows = strings.map(row(content:))

        return contentRows
            .innerJoin(by: inside)
            .encapsulated(firstElement: top, secondElement: bottom)
            .map { $0.joined() }
            .innerJoin(by: characterSet.newLine)
            .joined()
    }

}

extension Table {

    func line(with columnWidths: [Int], type: LineType) -> [String] {

        var insideSymbol: String
        var leftSymbol: String
        var rightSymbol: String

        switch type {
        case .inside:
            insideSymbol    = characterSet.horisontalVertical
            leftSymbol      = characterSet.rightHorisontal
            rightSymbol     = characterSet.leftHorisontal
        case .top:
            insideSymbol    = characterSet.downHorisontal
            leftSymbol      = characterSet.topLeftCorner
            rightSymbol     = characterSet.topRightCorner
        case .bottom:
            insideSymbol    = characterSet.upHorisontal
            leftSymbol      = characterSet.bottomLeftCorner
            rightSymbol     = characterSet.bottomRightCorner
        }

        return columnWidths
            .map { value in
                String(repeating: characterSet.horisontal, count: value)
            }
            .innerJoin(by: characterSet.horisontal + insideSymbol + characterSet.horisontal)
            .encapsulated(
                firstElement: leftSymbol + characterSet.horisontal,
                secondElement: characterSet.horisontal + rightSymbol
            )
    }

    func row(content: [String]) -> [String] {
        content
            .innerJoin(by: characterSet.space + characterSet.vertical + characterSet.space)
            .encapsulated(
                firstElement: characterSet.vertical + characterSet.space,
                secondElement: characterSet.space + characterSet.vertical
            )
    }

}

extension Table {

    typealias Info = ContentInfo

    func printInfo(
        for data: [CustomStringConvertible],
        elementsCount: Int
    ) -> Info {
        let filledStrings = data.map(String.init(describing:))
        let extraStrings = Array(repeating: characterSet.space, count: elementsCount - data.count)
        let allStrings = filledStrings + extraStrings

        return Info(
            rowStrings: allStrings,
            widths: allStrings.map(\.count)
        )
    }

}

extension Table {

    func normalisedData(
        data: [[CustomStringConvertible]]
    ) -> [[CustomStringConvertible]] {
        let maxElementsInRow = data
            .map(\.count)
            .reduce(0, max)

        let normalisedData: [[CustomStringConvertible]] = data
            .map { semiData in
                let filledStrings: [CustomStringConvertible] = semiData.map(String.init(describing:))
                let extraStrings: [CustomStringConvertible] = Array(repeating: characterSet.space, count: maxElementsInRow - semiData.count)
                let allStrings: [CustomStringConvertible] = filledStrings + extraStrings

                return allStrings
            }

        return normalisedData
    }
}

extension Table {

    func accessoriedData(
        data: [[CustomStringConvertible]]
    ) -> [[CustomStringConvertible]] {

        let maxElementsInRow = data
            .map(\.count)
            .reduce(0, max)

        let maxElementsInColumn = data
            .count

        let accessoryColumn = accesssoryColumn(amountOfRows: maxElementsInColumn)
        let accessoryRow    = accesssoryRow(amountOfColumns: maxElementsInRow)


        var data: [[CustomStringConvertible]] = data

        data = data
            .enumerated()
            .map { rowIndex, rowElements in
                let rowAcesssory = accessoryColumn.flatMap { [$0[rowIndex]] } ?? []
                return rowAcesssory + rowElements
            }

        data = accessoryRow
            .flatMap { accessoryRow in
                [accessoryRow] + data
            } ?? data

        return data
    }

    func accesssoryColumn(
        amountOfRows amount: Int
    ) -> [CustomStringConvertible]? {
        switch self.accessory.assideAccessory {
        case .number:
            return Array(1...amount)
        case .anything(let something):
            let elementsCount = something.count
            return (elementsCount >= amount) ? something : something + Array(repeating: characterSet.space, count: amount - elementsCount)
        case .none:
            return nil
        }
    }

    func accesssoryRow(
        amountOfColumns amount: Int
    ) -> [CustomStringConvertible]? {
        let result: [CustomStringConvertible]?
        let shouldUseDivider: Bool = accessory.assideAccessory.hasAccessory

        switch self.accessory.headerAccessory {
        case .number:
            result = Array(1...amount)
        case .anything(let something):
            let elementsCount = something.count
            result = (elementsCount >= amount) ? something : something + Array(repeating: characterSet.space, count: amount - elementsCount)
        case .none:
            result = nil
        }

        return result.flatMap { rowAceessory in
            if shouldUseDivider {
                return [accesssoryDivider()] + rowAceessory
            } else {
                return rowAceessory
            }
        }
    }

    func accesssoryDivider() -> CustomStringConvertible {
        switch self.accessory.divideAccessory {
        case .anything(let something):
            return something
        case .backslash:
            return "\\"
        case .none:
            return " "
        }
    }

}
