//
//  Array+Extensions.swift
//  
//
//  Created by Artem Myshkin on 04.03.2022.
//

import Foundation

extension Array {

    func encapsulated(
        firstElement: Element,
        secondElement: Element
    ) -> Array<Element> {
        [
            [firstElement],
            self,
            [secondElement],
        ]
            .flatMap { $0 }
    }

    func encapsulated(by element: Element) -> Array<Element> {
        self.encapsulated(firstElement: element, secondElement: element)
    }

    func innerJoin(
        by element: Element
    ) -> Array<Element> {
        let s = self
            .map(CollectionOfOne.init)
            .joined(separator: CollectionOfOne(element))
            .compactMap { $0 }
        return Array(s)
    }

}
