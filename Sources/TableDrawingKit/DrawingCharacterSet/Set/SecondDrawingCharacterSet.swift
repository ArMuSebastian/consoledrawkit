//
//  SecondDrawingCharacterSet.swift
//  
//
//  Created by Artem Myshkin on 04.03.2022.
//

import Foundation

internal struct SecondDrawingCharacterSet: TableDrawingCharacterSet {

    let horisontal = "-"
    let vertical = "|"

    let horisontalVertical = "+"

    let downHorisontal = "-"
    let upHorisontal = "-"
    let rightHorisontal = "|"
    let leftHorisontal = "|"

    let topRightCorner = "*"
    let topLeftCorner = "*"
    let bottomRightCorner = "*"
    let bottomLeftCorner = "*"

    let space = " "
    let newLine = "\n"

}
