//
//  DrawingCharacterSet.swift
//  
//
//  Created by Artem Myshkin on 04.03.2022.
//

import Foundation

extension Table {

    public enum DrawingCharacterSet {

        case first
        case second

        internal var characterSet: TableDrawingCharacterSet {
            switch self {
            case .first:
                return FirstDrawingCharacterSet()
            case .second:
                return SecondDrawingCharacterSet()
            }
        }

    }

}
