//
//  DrawingCharacterSet.swift
//  
//
//  Created by Artem Myshkin on 04.03.2022.
//

import Foundation

protocol TableDrawingCharacterSet {

    var horisontal: String { get }
    var vertical: String { get }

    var horisontalVertical: String { get }

    var downHorisontal: String { get }
    var upHorisontal: String { get }
    var rightHorisontal: String { get }
    var leftHorisontal: String { get }

    var topRightCorner: String { get }
    var topLeftCorner: String { get }
    var bottomRightCorner: String { get }
    var bottomLeftCorner: String { get }

    var space: String { get }
    var newLine: String { get }

}
