//
//  AssideAccessory.swift
//  
//
//  Created by Artem Myshkin on 04.03.2022.
//

import Foundation

extension Table {

    public enum AssideAccessory {

        case number
        case anything(something: [CustomStringConvertible])
        case none

        var hasAccessory: Bool {
            switch self {
            case .number:
                return true
            case .anything:
                return true
            case .none:
                return false
            }
        }

    }

}
