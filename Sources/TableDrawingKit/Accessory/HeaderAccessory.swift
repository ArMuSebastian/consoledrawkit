//
//  HeaderAccessory.swift
//  
//
//  Created by Artem Myshkin on 04.03.2022.
//

import Foundation

extension Table {

    public enum HeaderAccessory {

        case number
        case anything(something: [CustomStringConvertible])
        case none

    }

}
