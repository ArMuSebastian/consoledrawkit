//
//  Accessory.swift
//  
//
//  Created by Artem Myshkin on 04.03.2022.
//

import Foundation

extension Table {

    public struct Accessory {

        public typealias HeaderAccessory   = Table.HeaderAccessory
        public typealias AssideAccessory   = Table.AssideAccessory
        public typealias DivideAccessory   = Table.DivideAccessory

        internal var headerAccessory: HeaderAccessory
        internal var assideAccessory: AssideAccessory
        internal var divideAccessory: DivideAccessory

        public init(
            headerAccessory: HeaderAccessory,
            assideAccessory: AssideAccessory,
            divideAccessory: DivideAccessory
        ) {
            self.headerAccessory = headerAccessory
            self.assideAccessory = assideAccessory
            self.divideAccessory = divideAccessory
        }

    }

}

extension Table.Accessory {

    public init() {
        self.init(
            headerAccessory: .none
        )
    }

    public init(
        assideAccessory: AssideAccessory
    ) {
        self.init(
            headerAccessory: .none,
            assideAccessory: assideAccessory
        )
    }

    public init(
        headerAccessory: HeaderAccessory
    ) {
        self.init(
            headerAccessory: headerAccessory,
            assideAccessory: .none
        )
    }

    public init(
        headerAccessory: HeaderAccessory,
        assideAccessory: AssideAccessory
    ) {
        self.init(
            headerAccessory: headerAccessory,
            assideAccessory: assideAccessory,
            divideAccessory: .none
        )
    }

}
