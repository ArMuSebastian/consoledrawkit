//
//  DivideAccessory.swift
//  
//
//  Created by Artem Myshkin on 04.03.2022.
//

import Foundation

extension Table {

    public enum DivideAccessory {

        case backslash
        case anything(something: CustomStringConvertible)
        case none

    }

}
