//
//  Line.swift
//  
//
//  Created by Artem Myshkin on 04.03.2022.
//

import Foundation

extension Table {

    enum LineType {

        case inside
        case top
        case bottom

    }

}
