//
//  ContentInfo.swift
//  
//
//  Created by Artem Myshkin on 04.03.2022.
//

import Foundation

extension Table {

    struct ContentInfo {

        var rowStrings: [String]
        var widths: [Int]

    }

}
